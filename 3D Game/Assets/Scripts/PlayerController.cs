﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float forwardForce;
    public float sidewaysForce;
    private Player player;

    private void Start()
    {
        player = new Player(gameObject);
    }

    private void FixedUpdate()
    {
        player.Rigidbody.AddForce(0, 0, forwardForce * Time.deltaTime);

        if (Input.GetKey(KeyCode.A)) player.Rigidbody.AddForce(-sidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        if (Input.GetKey(KeyCode.D)) player.Rigidbody.AddForce(sidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
    }

    public void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Obstacle"))
        {
            forwardForce = 0f;
            sidewaysForce = 0f;
            GameEvents.reference.NotifyGameEvent(GameEventTypes.PlayerDied);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        GameEvents.reference.NotifyGameEvent(GameEventTypes.PlayerWon);
    }
}