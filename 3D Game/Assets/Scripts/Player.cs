﻿using UnityEngine;

public class Player
{
    public Player(GameObject gameObjectRef)
    {
        Reference = this;
        GameObjectRef = gameObjectRef;
        Rigidbody = GameObjectRef.GetComponent<Rigidbody>();
        Transform = GameObjectRef.GetComponent<Transform>();
    }

    public static Player Reference { get; set; }
    public GameObject GameObjectRef { get; }
    public Rigidbody Rigidbody { get; }
    public Transform Transform { get; }
}