﻿using UnityEngine;

public class AnimationEvents : MonoBehaviour
{
    public void NotifyAnimationEvent(GameEventTypes gameEventTypes)
    {
        /*SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);*/
        GameEvents.reference.NotifyGameEvent(gameEventTypes);
    }
}