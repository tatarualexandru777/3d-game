﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject levelCompleteUI;
    public GameObject gameOverUI;
    public GameObject buttonUI;

    private void Start()
    {
        GameEvents.reference.Subject += OnEventTriggered;
    }

    public void OnEventTriggered(GameEventTypes gameEventTypes)
    {
        switch (gameEventTypes)
        {
            case GameEventTypes.PlayerDied:
                GameOver();
                break;
            case GameEventTypes.PlayerWon:
                LevelComplete();
                break;
            case GameEventTypes.ShowButtons:
                ShowUIButtons();
                break;
            default:
                return;
        }
    }

    public void GameOver()
    {
        gameOverUI.SetActive(true);
    }

    public void LevelComplete()
    {
        levelCompleteUI.SetActive(true);
    }

    public void ShowUIButtons()
    {
        buttonUI.SetActive(true);
        Time.timeScale = 0;
    }
}