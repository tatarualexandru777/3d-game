﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Vector3 cameraOffset;

    private void Update()
    {
        transform.position = Player.Reference.Transform.position + cameraOffset;
    }
}