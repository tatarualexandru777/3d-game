﻿using System;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
    public static GameEvents reference;
    public event Action<GameEventTypes> Subject;

    private void Awake()
    {
        reference = this;
    }
    public void NotifyGameEvent(GameEventTypes gameEventType)
    {
        Subject?.Invoke(gameEventType);
    }
}
