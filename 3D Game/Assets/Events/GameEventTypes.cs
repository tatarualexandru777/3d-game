﻿public enum GameEventTypes
{
    PlayerDied,
    PlayerWon,
    ShowButtons
}